/**
 * 
 */
package com.ids.project.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ids.project.blog.entities.Article;
import com.ids.project.blog.services.ArticleService;



/**
 * @author Felipe Daza
 *
 */
@RestController
@CrossOrigin
public class ArticleController {
	
	@Autowired
	private ArticleService service;
	
	@GetMapping("/api/getarticles")
	public List<Article> queryAllArticles(){
		
		List<Article> articles = service.queryAllArticles();
		return articles;
	}
	
	@GetMapping("/api/getarticle/{titleAuthor}")
	public Article consultArticle(@PathVariable(name="titleAuthor") String titleAuthor) {
		
	
		List<Article> article = service.queryAllArticles();
		Article response_article = null;
		
		for(Article art : article) {
			if(art.getTitle().equalsIgnoreCase(titleAuthor) || art.getAuthor().equalsIgnoreCase(titleAuthor)) {
				response_article = service.consultArticle(art.getId());
			}
		}
		return response_article;	
	}
	
	@PostMapping("/api/postarticle")
	public void saveArticle(@RequestBody Article article) {
		
		Article response_article = consultArticle(article.getTitle());
		
		if(response_article == null) {
			service.saveArticle(article);
		}else {
			response_article.setAuthor(article.getAuthor());
			response_article.setTitle(article.getTitle());
			response_article.setDate(article.getDate());
			service.updateArticle(response_article);
		}
	}
	
	@DeleteMapping("/api/delarticle/{title}")
	public void deleteArticle(@PathVariable(name="title") String title) {
		Article article = consultArticle(title);
		service.deleteArticle(article.getId());
	}
	
	@PutMapping("/api/putarticle/{title}")
	public void updateArticle(@RequestBody Article article, @PathVariable(name="title") String title) {
		Article response_article = consultArticle(title);
		if(response_article != null) {
			response_article.setAuthor(article.getAuthor());
			response_article.setTitle(article.getTitle());
			response_article.setDate(article.getDate());
			service.updateArticle(response_article);
		}
	}
}