/**
 * 
 */
package com.ids.project.blog.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.ids.project.blog.entities.User;
import com.ids.project.blog.services.UserService;



/**
 * @author Felipe Daza
 *
 */
@RestController
@CrossOrigin
public class UserController {
	
	@Autowired
	private UserService service;
	
	@GetMapping("/api/users")
	public List<User> usersAll(){
		List<User> users = service.allUsers();
		return users;
	}
	
	@GetMapping("/api/user/{userName}")
	public User queryAlumno(@PathVariable(name="userName") String userName) {
		
		List<User> user = service.allUsers();
		User response = null;
		
		for(User us : user) {
			if(us.getName().equals(userName)) {
				response = us;
				break;
			}
		}
		return response;
	}
	
}