/**
 * 
 */
package com.ids.project.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ids.project.blog.entities.Article;


/**
 * @author Felipe Daza
 *
 */
public interface ArticleRepository extends JpaRepository<Article, Long>{
	
}

