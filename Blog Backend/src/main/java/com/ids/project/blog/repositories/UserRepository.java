/**
 * 
 */
package com.ids.project.blog.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ids.project.blog.entities.User;

/**
 * @author Felipe Daza
 *
 */
public interface UserRepository extends JpaRepository<User, Long> {

}
