/**
 * 
 */
package com.ids.project.blog.services;

import java.util.List;

import com.ids.project.blog.entities.Article;


/**
 * @author Felipe Daza
 *
 */
public interface ArticleService {
	List<Article> queryAllArticles();
	Article consultArticle(Long idArticle);
	void saveArticle(Article article);
	void deleteArticle(Long idArticle);
	void updateArticle(Article article);

}
