/**
 * 
 */
package com.ids.project.blog.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ids.project.blog.entities.Article;
import com.ids.project.blog.repositories.ArticleRepository;


/**
 * @author Felipe Daza
 *
 */
@Service
public class ArticleServiceImpl implements ArticleService{
	
	@Autowired
	private ArticleRepository repository;  

	@Override
	public List<Article> queryAllArticles() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}


	@Override
	public Article consultArticle(Long idArticle) {
		// TODO Auto-generated method stub
		
		java.util.Optional<Article> article = repository.findById(idArticle);
		
		if(article.isPresent()) {
			return article.get();
		}
		return null;
	}

	@Override
	public void saveArticle(Article article) {
		// TODO Auto-generated method stub
		repository.save(article);
		
	}

	@Override
	public void deleteArticle(Long idArticle) {
		// TODO Auto-generated method stub
		repository.deleteById(idArticle);
		
	}

	@Override
	public void updateArticle(Article article) {
		// TODO Auto-generated method stub
		repository.save(article);
		
	}


}