/**
 * 
 */
package com.ids.project.blog.services;

import java.util.List;

import com.ids.project.blog.entities.User;



/**
 * @author Felipe Daza
 *
 */
public interface UserService {
	List<User> allUsers();
}
