/**
 * 
 */
package com.ids.project.blog.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ids.project.blog.entities.User;
import com.ids.project.blog.repositories.UserRepository;

/**
 * @author Felipe Daza
 *
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository repository; 
	
	@Override
	public List<User> allUsers() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

}
