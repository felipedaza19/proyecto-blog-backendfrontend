var btnsignin = document.getElementById('btn-signin');
var eventSignin = new Event('eventSignin');

var btnregister = document.getElementById('btn-register');
var eventRegister = new Event('eventRegister');

var btnconsult = document.getElementById('btn-consult');
var eventConsult = new Event('eventConsult');

var btnclose = document.getElementById('btn-close');
var eventClose = new Event('eventClose');

var modal = document.getElementById('modal');



MVC.Controller = class Controller{
		constructor(props){

			//Que escuche los eventos primero antes de crear el model que es el que envia la peticion
			this.eventHandler();
			this.modelUser = new props.model(props.endpoint);
			this.model = new props.model(props.endpointsetarticle);
			this.modelArticles = new props.model(props.endpointgetarticles);
			this.modelArticle = new props.model(props.endpointsetarticle);
			this.view  = new props.view(props.contentElement);		
		}
	
	//Se encarga de evaluar cada uno de los eventos de los botones
	eventHandler(){
		if(btnsignin){
			btnsignin.addEventListener('eventSignin', () => {
				this.getDataUser();
			});	
		}
		
		if(btnregister){
			btnregister.addEventListener('eventRegister', ()=>{
				this.setDataArticle();
			});
		}
		
		document.body.addEventListener('onloadApp', () => {
			this.getDataArticles();
		}); 

		if(btnconsult){
			btnconsult.addEventListener('eventConsult', () => {
				modal.setAttribute('open','true');
				let titleAuthor = document.getElementById('consult').value;
				this.getArticle(titleAuthor);
			});
		}
		 if(btnclose){
			btnclose.addEventListener('eventClose', () =>{
				modal.removeAttribute('open');
			});
		}
	}

	//Recupera  todos los articulos y los presenta a la vista
	getDataArticles(){
		this.modelArticles.getArticles()
		.then(data => {
			this.view.notify(data);	
		})
		
	}

	getArticle(titleAuthor){

		this.modelArticles.getArticles()
		.then(data => {
			let response = {};

			for(let key in data){	

				if (data[key].title == titleAuthor || data[key].author == titleAuthor) {
					this.view.updateModal(data[key]);
					break;
				}
			}
		})
	}



	//Recupera todos los usuarios y los compara con los asigando por el usuario
	getDataUser(){

		let nameUser = document.getElementById("name").value;
		let passwordUser = document.getElementById("password").value;

		this.modelUser.getUser()
		.then(data => {
			
			for(let key in data){	

				if (data[key].name == nameUser && data[key].password == passwordUser) {
					window.location="./templates/register.html";
				}
			}
		})
		.catch(console.log);
	}

	//Persiste un articulo, se los recupera de la interfaz grafica y se los envia al modelo
	//para que este los guarde

	setDataArticle(){
		let titleArt = document.getElementById('title').value;
		let authorArt = document.getElementById('author').value;
		let dateArt = document.getElementById('date').value;
		let keywordsArt = document.getElementById('keywords').value;
		let contentArt = document.getElementById('content').value;

		var data = {
			title: titleArt,
			author:authorArt,
			date: dateArt,
			keywords: keywordsArt,
			content: contentArt
		};
		
		var setData = JSON.stringify(data);
		this.model.setArticle(setData,this.model.endpoint );
	}
}