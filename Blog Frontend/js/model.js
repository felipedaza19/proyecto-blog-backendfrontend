MVC.Model = class Model{
	constructor(endpoint){
		this.endpoint = endpoint;
		this.modelData = {};
	}
	
	//Esta secciòn de coidgo obtiene todos los usuarios de la base de datos
	getUser(){
		return fetch(`${this.endpoint}`)
		.then(resp => {
			if(resp.ok){
				return resp.json();
			}

			return Error("No se pudieron obtener los datos");
		})
	
		.then(data => {
			this.setModelo(data);
			return data;
		})
	}

	//Esta secciòn de coidgo obtiene todos los registros de articulos de la base de datos
	getArticles(){
		return fetch('http://localhost:8080/api/getarticles')
		.then(resp => {
			if(resp.ok){
				return resp.json();
			}

			return Error("No se pudieron obtener los datos");
		})
	
		.then(data => {
			this.setModelo(data);
			return data;
		})
	}

	//Esta secciòn de coidgo registra un articulo en la base de datos
	setArticle(data, endpointsetarticle){
       	return fetch(endpointsetarticle, {
           method: 'POST',
           headers: new Headers({
               'Content-Type': 'application/json;charset=UTF-8'
           }),
           body: data,
           cache: 'no-cache'
       	})
       	.catch(err => console.log(err))
	}

	setModelo(data){
		this.modelData = data;
	}

	getModelo(){
		return this.modelData;	
	}
}