MVC.View = class View{
		constructor(elem){
			//que escuche primero los eventos
			this.eventHandler();
			this.elem=elem;
			
		}
	

	eventHandler(){
		document.body.addEventListener('onLoadData',(event) => {
		this.updateView(event.detail);

		});
	}

	notify(data){

		const onLoadDataEvent = new CustomEvent("onLoadData", {detail:data, bubbles:true});
		if(onLoadDataEvent){
			this.elem.dispatchEvent(onLoadDataEvent);
		}	
	}

	//Presenta los datos de un articulo en una fila de un tbody
	updateView (datos){
		for(let key in datos){
			this.elem.insertAdjacentHTML('beforeend',`
				
        		<tr>	
        			<td id="title">${datos[key].title}</td>
        			<td id="author">${datos[key].author}</td>
        			<td id="keywords">${datos[key].keywords}</td>
        			<td id="content">${datos[key].content}</td>
        			<td id="date">${datos[key].date}</td>	
        		</tr>
        	`);
		}
	}

	updateModal(datos){
		
		var sectionmodaldata = document.getElementById('modaldata');

		sectionmodaldata.insertAdjacentHTML('afterbegin',`

				<P>ARTICLE MANAGEMENT</P>		    
		        <label>Write the title of the article</label><br>
				<input type="text" id="title" class="form-control">${datos.title}</input><br>
				<label>Write the author of the article</label><br>
			    <input type="text" id="author" class="form-control">${datos.author}</input><br>
			    <label>Enter the date of publication of the article</label><br>
			    <input type="date" id="date" class="form-control">${datos.date}</input><br>
			    <label>Keywords</label><br>
			    <input type="text" id="keywords" class="form-control">${datos.keywords}</input><br>
			    <label>Content</label><br>
			    <textarea id="content" rows="30" cols="66" class="form-control">${datos.content}</textarea><br>
        	`);
	}
}
