MVC.controllerInstance = new MVC.Controller({
	model:MVC.Model,
	view:MVC.View,
	contentElement: document.querySelector('#tbody'),
	contentElementModal: document.querySelector('#modal'),
	endpoint: 'http://localhost:8080/api/users',
	endpointsetarticle: 'http://localhost:8080/api/postarticle',
	endpointgetarticles: 'http://localhost:8080/api/getarticles',
	endpointgetarticle:'http://localhost:8080/api/getarticle/{titleAuthor}'
});	

document.body.dispatchEvent(new Event('onloadApp'));
